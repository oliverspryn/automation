// Creates the necessary users for Home Assistant and
// grants them the correct privileges

CREATE USER homeassistant@'%' IDENTIFIED BY 'nope';
CREATE SCHEMA homeassistant;

GRANT ALL PRIVILEGES ON homeassistant.* TO homeassistant@'%';
QUIT;
