#!/bin/bash

# A general guide showing the important aspects of how the automation server
# is setup for future reference. Running this script from the very beginning
# certainly won't completely setup the server. It is more of a roadmap.

# region Mount the relevant NAS shares

sudo apt-get update
sudo apt-get install nfs-common -y

sudo mkdir -p /nfs/Docker
sudo nano /etc/fstab
# Use modified fstab file here
sudo reboot

# endregion

# region Install Docker
# Per: https://docs.docker.com/engine/install/ubuntu/

sudo apt-get install \
  apt-transport-https \
  ca-certificates \
  curl \
  gnupg-agent \
  software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88

sudo add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable"

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io

sudo groupadd docker
sudo usermod -aG docker ${USER}

# endregion

# region Install Docker Compose
# Per: https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04

sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +d /usr/local/bin/docker-compose
docker-compose --version

# endregion

# region Install MySQL
# Per: https://hub.docker.com/_/mysql/

docker pull mysql
sudo mkdir -p /nfs/Swap/mysql
sudo chown -R ${USER}:docker /nfs/Swap/mysql
sudo chmod -R 755 /nfs/Swap/mysql

docker run --name mysql \
  -v /nfs/Swap/mysql:/var/lib/mysql \
  -e MYSQL_ROOT_PASSWORD=nope \
  -p 3306:3306 \
  -d \
  --restart unless-stopped \
  mysql

docker exec -it mysql sh
# Once inside the MySQL container
mysql -u root -p
# Run homeassistant-mysql.sql here

# endregion

# region Install InfluxDB
# Per: https://hub.docker.com/_/influxdb

docker pull influxdb
sudo mkdir -p /nfs/Swap/influxdb
sudo chown -R ${USER}:docker /nfs/Swap/influxdb
sudo chmod -R 755 /nfs/Swap/influxdb

docker run --name influxdb \
  -v /nfs/Swap/influxdb:/var/lib/influxdb \
  -e INFLUXDB_ADMIN_USER=admin \
  -e INFLUXDB_ADMIN_PASSWORD=nope \
  -p 8086:8086 \
  -d \
  --restart unless-stopped \
  influxdb

docker exec -it influxdb sh
# Once inside the Influx container
influx -username admin -password nope
# Run homeassistant-influx.sql here

# endregion

# region Install HomeAssistant

docker pull homeassistant/home-assistant:stable
sudo mkdir -p /nfs/Docker/homeassistant

# Move all of the HomeAssistant files to the new folder
sudo chown -R ${USER}:docker /nfs/Docker/homeassistant
chmod -R 755 /nfs/Docker/homeassistant

docker run --name homeassistant
  -v /nfs/Docker/homeassistant:/config
  -v /nfs/Docker/letsencrypt:/certificates:ro
  -v /etc/localtime:/etc/localtime:ro
  --net=host
  -d
  --restart unless-stopped \
  homeassistant/home-assistant:stable

# Run through the setup wizard
# Install Hubitat per: https://github.com/jason0x43/hacs-hubitat

# endregion

# region Install OwnTracks
# Per: https://github.com/owntracks/docker-recorder

docker pull owntracks/recorder
sudo mkdir -p /nfs/Docker/owntracks

sudo chown -R ${USER}:docker /nfs/Docker/owntracks
chmod -R 755 /nfs/Docker/owntracks

docker run --name owntracks
  -v /nfs/Docker/owntracks:/store
  -p 8083:8083
  -d
  --restart unless-stopped
  owntracks/recorder

# endregion
