# Home Automation

A single location keep all of my automation projects, scripts, and setup procedures.

## Applications

These are all of the applications which run within this cluster.

**Core setup:**

- **[Caddy](https://caddyserver.com/):** Reverse proxy server to assign an externally-accessible (WAN) or interally-accessible (LAN) domain name to each application within the cluster. Also useful for accessing devices inside or outside of the network without needing to remember their unique IP addresses.
- **[CoreDNS](https://coredns.io/):** DNS server to provide interally-accessible (LAN) domain names to each application within the cluster, or to other devices on the network

**Data stores:**

- **[Redis](https://redis.io/):** In-memory cache storage for several applications

**User-facing applications:**

## Tools

These are the tools that you'll need in order to spin up a cluster on your own server:

- **[Docker](https://docker.com/):** Engine which runs each application within this cluster
- **[Docker Compose](https://docs.docker.com/compose/):** Orchestration application to spin up and down the entire cluster with ease and without conflicts
- **[Doppler CLI](https://doppler.com/):** Secrets manager. Goodness knows there are a LOT of secrets in this cluster.

All of these tools can be automatically installed by running `./server/setup.sh`.

## Architecture


