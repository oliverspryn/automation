# Plex

By and large, Plex is very easy to setup. The offical GitHub repository for the Plex Docker image is here: [https://github.com/plexinc/pms-docker](https://github.com/plexinc/pms-docker). Please note that this installation is a two-step process. First, is the Docker container itself, and second is the networking and firewall setup.

## Container Setup

To setup the server for my instance:

1. I opted to use Bridge Networking, and thus followed the directions here: [https://github.com/plexinc/pms-docker#bridge-networking](https://github.com/plexinc/pms-docker#bridge-networking)
1. For the `TZ` environment variable, fetch the correct timezone name from here: [https://en.wikipedia.org/wiki/List_of_tz_database_time_zones](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)
1. For the `PLEX_CLAIM`, fetch a token from here: [https://www.plex.tv/claim](https://www.plex.tv/claim)
1. For the `ADVERTISE_IP`, just supply a value like this: `http://my.internal.ip.address:32400/`

## Network and Firewall

Since my container runs in bridge mode, it is not able to setup the port forwarding automatically. Thus, these steps must be followed:

1. Log into your router
1. Forward the WAN port 32400 to the `ADVERTISE_IP` port of 32400 on your LAN
