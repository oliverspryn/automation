// Creates the necessary users for Nextcloud and
// grants them the correct privileges

CREATE USER nextcloud@'%' IDENTIFIED BY 'nope';
CREATE SCHEMA nextcloud;

GRANT ALL PRIVILEGES ON nextcloud.* TO nextcloud@'%';
QUIT;
